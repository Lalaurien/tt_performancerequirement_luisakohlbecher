﻿using UnityEngine;


namespace TTPP
{
    public class Item : MonoBehaviour
    {

        private int weight;


        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }
    }
}