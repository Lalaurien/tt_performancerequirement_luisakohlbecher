﻿using UnityEngine;
using System.Collections;

namespace TTPP
{
    public class ItemManager : MonoBehaviour
    {

        public Item item;

        private void Start()
        {
            SetItemWeight();
            Debug.Log(message: "Current Item Weight: " + item.Weight);
        }

        private void SetItemWeight()
        {
            item.Weight = 5;
        }
        
    }
}
