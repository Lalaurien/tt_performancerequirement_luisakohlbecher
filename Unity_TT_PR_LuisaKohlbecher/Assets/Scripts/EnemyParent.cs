﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTPP
{ 

public class EnemyParent : MonoBehaviour
{
   private bool isAlive = true;
   private  int health = 150;

    public void Death()
    {
        if (health <= 0)
        {
            isAlive = false;
            Debug.Log(message: "Enemy has died");
        }

    }

    public void Update()
    {
        Death();
    }


}
}
