﻿using UnityEngine;


namespace TTPP
{

    public class Player : MonoBehaviour
    {
       private int health = 200;
       private string name = "Tim";
       private bool isAlive = true;
       private int mana = 50;
       private bool isJumping;
       private int maxHealth = 225;

        public void IsDead()
        {
            if (health <= 0)
            {
                isAlive = false;
                Debug.Log(message: "Player has died");
            }

        }
     

        public void InitHealth()
        {
            health = maxHealth;
        }

        public void ChangeHealth(int healthValue)
        {
            health = healthValue;
        }

        public int GetHealth()
        {
            return health;
        }


    }
}